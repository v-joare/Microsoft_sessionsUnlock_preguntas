
$ (document).ready(function() {
        // $( "#myDiv" ).hide('fast');
        $('.panel-heading').on("click", function (event) {
           $(this).find('.glyphicon-menu-down').toggleClass('rotate');
        });
        $('#share_general').bind('click', function(e){
          window.open(url,height=100,width=150);
       });
        $( "#bus" ).keyup(function( event ) {
          $( "#accordion" ).fadeOut('slow');
          $( ".pager" ).fadeOut('slow');
          $( "#myDiv" ).fadeIn('slow');

       });
        $('.buscador').on("click", function (event) {
         $('input[name=bus]').fadeToggle('fast');

      });
        var byRow = $('body').hasClass('test-rows');
        $('.panel-group').each(function() {
          $(this).find('.panel-heading').matchHeight({
            byRow: byRow
         });
       });
     });
function obtenerDatos(){
   var byRow = $('body').hasClass('test-rows');
   $('.panel-group').each(function() {
     $(this).find('.panel-heading').matchHeight({
       byRow: byRow
    });
  });
}
function loadXMLDoc(){

  var xmlhttp;

  var n=document.getElementById('bus').value;

  if (window.XMLHttpRequest)
    {// code for IE7+, Firefox, Chrome, Opera, Safari
     xmlhttp=new XMLHttpRequest();
  }
  else
    {// code for IE6, IE5
     xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function()
  {
     if (xmlhttp.readyState==4 && xmlhttp.status==200)
     {
        document.getElementById("myDiv").innerHTML=xmlhttp.responseText;
     }
  }
  xmlhttp.open("POST","resultado.php",true);
  xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
  xmlhttp.send("buscar="+n);
  setTimeout(function(){
   obtenerDatos();
   paginador();
},2000);

}

function mostrarpaginador(){
 document.getElementById("pager2").style.display = "block";
}
function paginador(){
 document.getElementById("pager2").innerHTML="";
$.fn.pageMe2 = function(opts){
var $this = this,
    defaults = {
        showPrevNext: true,
        hidePageNumbers: false,
        showFirstLast: false
    },
    settings = $.extend(defaults, opts);

var listElement = $this;
var perPage = settings.perPage; 
var children = listElement.children();
var pager = $('.pagination');

if (typeof settings.childSelector!="undefined") {
    children = listElement.find(settings.childSelector);
}

if (typeof settings.pagerSelector!="undefined") {
    pager = $(settings.pagerSelector);
}

var numItems = children.size();
var numPages = Math.ceil(numItems/perPage);

pager.data("curr",0);

if (settings.showFirstLast){
    $('<li><a href="#" class="first_link"><</a></li>').appendTo(pager);
}     
if (settings.showPrevNext){
    $('<li><a href="#" class="prev_link">« Anterior</a></li>').appendTo(pager);
}

var curr = 0;
while(numPages > curr && (settings.hidePageNumbers==false)){
    $('<li><a href="#" class="pagination_12">'+(curr+1)+'</a></li>').appendTo(pager);
    curr++;
}

if (settings.numbersPerPage>1) {
   $('.pagination_12').hide();
   $('.pagination_12').slice(pager.data("curr"), settings.numbersPerPage).show();
}

if (settings.showPrevNext){
    $('<li><a href="#" class="next_link">Siguiente »</a></li>').appendTo(pager);
}
if (settings.showFirstLast){
    $('<li><a href="#" class="last_link">></a></li>').appendTo(pager);
}  

// pager.find('.pagination_12:first').addClass('active');
pager.find('.prev_link').hide();
// if (numPages<=1) {
//     pager.find('.next_link').hide();
// }
pager.children().eq(1).addClass("active");

children.hide();
children.slice(0, perPage).show();

pager.find('li .pagination_12').click(function(){
    var clickedPage = $(this).html().valueOf()-1;
    goTo(clickedPage,perPage);
    return false;
});
pager.find('li .first_link').click(function(){
    first();
    return false;
});  

pager.find('li .prev_link').click(function(){
    previous();
    return false;
});
pager.find('li .next_link').click(function(){
    next();
    return false;
});
pager.find('li .last_link').click(function(){
    last();
    return false;
});    
function previous(){
    var goToPage = parseInt(pager.data("curr")) - 1;
    goTo(goToPage);
}

function next(){
    goToPage = parseInt(pager.data("curr")) + 1;
    goTo(goToPage);
}

function first(){
    var goToPage = 0;
    goTo(goToPage);
} 

function last(){
    var goToPage = numPages-1;
    goTo(goToPage);
} 

function goTo(page){
    var startAt = page * perPage,
        endOn = startAt + perPage;

    children.css('display','none').slice(startAt, endOn).show();

    if (page>=1) {
        pager.find('.prev_link').show();
    }
    else {
        pager.find('.prev_link').hide();
    }

if (page < (numPages - settings.numbersPerPage)) {
        pager.find('.next_link').show();
    }
    else {
        pager.find('.next_link').hide();
    }

    pager.data("curr",page);

if (settings.numbersPerPage > 1) {
    $('.pagination_12').hide();

    if (page < (numPages - settings.numbersPerPage)) {
        $('.pagination_12').slice(page, settings.numbersPerPage + page).show();
    }
    else {
        $('.pagination_12').slice(numPages-settings.numbersPerPage).show();
    }
}

    pager.children().removeClass("active");
    pager.children().eq(page+1).addClass("active");

}
};
$(document).ready(function(){

 $('#resultado_panel').pageMe2({pagerSelector:'.pager2',showPrevNext:true,hidePageNumbers:false,perPage:2});

}); 
}

$.fn.pageMe = function(opts){
var $this = this,
    defaults = {
        showPrevNext: true,
        hidePageNumbers: false,
        showFirstLast: false
    },
    settings = $.extend(defaults, opts);

var listElement = $this;
var perPage = settings.perPage; 
var children = listElement.children();
var pager = $('.pagination');

if (typeof settings.childSelector!="undefined") {
    children = listElement.find(settings.childSelector);
}

if (typeof settings.pagerSelector!="undefined") {
    pager = $(settings.pagerSelector);
}

var numItems = children.size();
var numPages = Math.ceil(numItems/perPage);

pager.data("curr",0);

if (settings.showFirstLast){
    $('<li><a href="#" class="first_link"><</a></li>').appendTo(pager);
}     
if (settings.showPrevNext){
    $('<li><a href="#" class="prev_link">« Anterior</a></li>').appendTo(pager);
}

var curr = 0;
while(numPages > curr && (settings.hidePageNumbers==false)){
    $('<li><a href="#" class="pagination_12">'+(curr+1)+'</a></li>').appendTo(pager);
    curr++;
}

if (settings.numbersPerPage>1) {
   $('.pagination_12').hide();
   $('.pagination_12').slice(pager.data("curr"), settings.numbersPerPage).show();
}

if (settings.showPrevNext){
    $('<li><a href="#" class="next_link">Siguiente »</a></li>').appendTo(pager);
}
if (settings.showFirstLast){
    $('<li><a href="#" class="last_link">></a></li>').appendTo(pager);
}  

// pager.find('.pagination_12:first').addClass('active');
pager.find('.prev_link').hide();
// if (numPages<=1) {
//     pager.find('.next_link').hide();
// }
pager.children().eq(1).addClass("active");

children.hide();
children.slice(0, perPage).show();

pager.find('li .pagination_12').click(function(){
    var clickedPage = $(this).html().valueOf()-1;
    goTo(clickedPage,perPage);
    return false;
});
pager.find('li .first_link').click(function(){
    first();
    return false;
});  

pager.find('li .prev_link').click(function(){
    previous();
    return false;
});
pager.find('li .next_link').click(function(){
    next();
    return false;
});
pager.find('li .last_link').click(function(){
    last();
    return false;
});    
function previous(){
    var goToPage = parseInt(pager.data("curr")) - 1;
    goTo(goToPage);
}

function next(){
    goToPage = parseInt(pager.data("curr")) + 1;
    goTo(goToPage);
}

function first(){
    var goToPage = 0;
    goTo(goToPage);
} 

function last(){
    var goToPage = numPages-1;
    goTo(goToPage);
} 

function goTo(page){
    var startAt = page * perPage,
        endOn = startAt + perPage;

    children.css('display','none').slice(startAt, endOn).show();

    if (page>=1) {
        pager.find('.prev_link').show();
    }
    else {
        pager.find('.prev_link').hide();
    }

if (page<=3) {
        pager.find('.next_link').show();
    }
    else {
        pager.find('.next_link').hide();
    }

    pager.data("curr",page);

if (settings.numbersPerPage > 1) {
    $('.pagination_12').hide();

    if (page < (numPages - settings.numbersPerPage)) {
        $('.pagination_12').slice(page, settings.numbersPerPage + page).show();
    }
    else {
        $('.pagination_12').slice(numPages-settings.numbersPerPage).show();
    }
}

    pager.children().removeClass("active");
    pager.children().eq(page+1).addClass("active");

}
};

$(document).ready(function(){

 $('#accordeon').pageMe({pagerSelector:'.pager',showPrevNext:true,hidePageNumbers:false,perPage:2});

});