<?php  
    include("conexion.php");
    $preguntaRespuesta= $con->query("SELECT * FROM faq_sessions ");
    // FUNCION QUE AGREGA EL LINK A LAS URL
    function findReplaceURL($respuesta){
        // The Regular Expression filter
        $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";     
        // Check if there is a url in the text
        if(preg_match($reg_exUrl, $respuesta, $url)) {        
               // make the urls hyper links
              return preg_replace($reg_exUrl, "<i><a href=".$url[0]." target='_blank'>haga click aquí</a></i> ", $respuesta);       
        } else {
               // if no urls in the text just return the text
               return $respuesta; 
        }
    } 
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="ms.loc" content="xl">
        <meta charset="UTF-8">
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="apple-mobile-web-app-capable" content="yes"/>
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <title>Unlock Data Insight | FAQs</title>
        <meta name="description" content="Mayor Seguridad Digital en la nube con Microsoft. Nuestros expertos le muestran cómo mejorar la seguridad en la nube con una plataforma ideal para usted.">
        <link rel="shortcut icon" type="image/x-icon" href="//www.microsoft.com/favicon.ico?v2">
        <link rel="stylesheet" type="text/css" href="assets/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="assets/reset.css">
        <link rel="stylesheet" type="text/css" href="assets/font_css.css" />
        <link rel="stylesheet" type="text/css" href="assets/style_type.css">
        <script src="assets/jquery.2.1.3.min.js"></script>
        <!--  JQUERY 2.1.3-->
        <script type="text/javascript" src="assets/jquery.matchHeight.js"></script>
        <!-- Slide -->
        <!-- Slide -->
        <meta property="og:title" content="Unlock Data Insight | Faqs"/>
        <meta property="og:site_name" content="microsoftsessions.com/Unlock_preguntas"/>
        <meta property="og:description" content=" Impulse la transformación de su negocio a través de sus datos. Nuestros expertos respondieron a sus preguntas. #MSSessions." />
        <meta property="og:image" content="https://www.microsoftsessions.com/empresas/images/img-share.png"/> 
        <meta property="og:url" content="https://www.microsoftsessions.com/unlock_preguntas"/>
        <style id="antiClickjack">body{display:none !important;}</style>
        <script type="text/javascript">
            if (self === top) {
                var antiClickjack = document.getElementById("antiClickjack");
                antiClickjack.parentNode.removeChild(antiClickjack);
            } else {
                top.location = self.location;
            }
        </script>

        
    </head>
    <body class="faqs">
     <!-- REDES SOCIALES -->
            <div class="social_desple">
                <span class="glyphicon2 glyphicon glyphicon-triangle-bottom"></span>
            </div>
            <div class="socialbar">
                <!-- COMPARTIR -->
                <a href="https://www.yammer.com/latamcio/messages/new?status=http%253A%252F%252Fwww.microsoftsessions.com/empresas/2F" target="_blank">
                    <img src="images/icon_yammer.png">
                </a>
                <a href="https://www.linkedin.com/shareArticle?url=https://www.microsoftsessions.com/preview_digital/unlock_preguntas&title=Unlock%20Data%20Insight%20|%20Faqs" target="_blank">
                    <img src="images/icon_linkedin.png">
                </a>
                 <a href="http://www.facebook.com/sharer.php?s=100&p[url]=https://www.microsoftsessions.com/preview_digital/unlock_preguntas
                 " target="_blank">
                    <img src="images/icon_facebook.png" id="share_general">
                </a>
                <a href="https://twitter.com/intent/tweet?hashtags=MSSessions&text=%20Impulse%20la%20transformación%20de%20su%20negocio%20a%20través%20de%20sus%20datos.%20Nuestros%20expertos%20respondieron%20a%20sus%20preguntas%20#MSSessions.&url=https://www.microsoftsessions.com/empresas/preview_digital" target="_blank">
                    <img src="images/icon_twitter.png">
                </a>
                <div class="vertical-text">COMPARTIR</div>
                <!-- FIN COMPARTIR -->
            </div>
            <!-- FIN REDES SOCIALES -->
        <!-- HEADER -->
    <div class="container-fluid header ">  
        <div class="container header_container"> 
            <div class="row">
                <div class="col-md-8 col-centered">
                <img src="images/img06.png" alt="MIcrosoft_ Session" class="center-block">
                    <h1 class="text-center">Preguntas frecuentes:</h1>   
                    <h3 class="text-center">las respuestas a todas sus consultas están aquí.</h3>
                </div>        
            </div>
            <div class="row">
                <div class="col-md-5 pull-right ms_s">
                    <p>#MSSessions</p>
                </div>
            </div>
        </div>
    </div>


    <!-- HEADER -->

    <!-- SEGUIRIDAD -->
    <section class="seguridad" id="seguridad">
        <div class="container">
            <div class="row">
               
            </div>
        </div>
    </section>
    <section class="faqs" id="faqs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                <div class="col-md-5 pull-right">
                    <input type="text" id="bus" name="bus"  placeholder="Encuentre FAQs" required onkeyup="loadXMLDoc()" style="display:none;" />

                    <span class="glyphicon glyphicon-search buscador  pull-right"></span>
                    </div>
                </div>
            </div> 
        </div> 
        <div class="container">
            <div id="myDiv">
                <div class="panel-group col-md-12" id="accordeon" role="tablist" aria-multiselectable="true">
                    <?php  
                    $contador =1;
                    while ($row = $preguntaRespuesta->fetch_assoc()){
                    $id= $row["idPregunta"];
                    $pregunta= $row["pregunta"];
                    $respuesta= $row["respuesta"];
                    if ($contador == 1 ) {
                        echo "<div class=' col-xs-12 col-md-6'>";
                    }
                    ?>
                    <div class="col-md-12"> 
                        <div class="panel panel-default panel_faqs">
                        <div class="panel-heading" role="tab" id="" data-toggle="collapse"  class="titulo" aria-expanded="true" data-parent="#accordion" href="#<?php echo($id);?>"  aria-controls="<?php echo($id);?>">
                            <h4 class="panel-title">
                                <a role="button" >
                                <?php echo($pregunta); ?> <span class="glyphicon glyphicon-menu-down"> </span>
                                </a>
                            </h4>
                        </div>
                        <div class="panel-collapse collapse out" role="tabpanel" id="<?php echo($id);?>" >
                            <div class="panel-body"><?php echo (findReplaceURL($respuesta)); ?></div>
                        </div>
                        </div>
                    </div>
                    <?php  
                    if ($contador == 5 ) {
                        echo "</div>";
                        $contador =  0;

                    }            
                    $contador++;
                    }
                    ?>
                </div>
                </div>
                <ul class="pager"></ul>
            </div>
        </div>
    </section>
    <footer>
        <div class="container foo-mobile">
            <div class="pull-right-1">
                <img src="images/logo_microsoft.png" alt="Powered by Microsoft Azure" width="104" >
                <p class="copy_right"> &copy; 2016 Microsoft</p>   
                <p class="copy_right">Este sitio está hosteado para Microsoft por Wunderman.</p>   
            </div>
            <div class="links-margin">
                <a href="http://go.microsoft.com/fwlink/p/?LinkID=511196" >Cont&aacute;ctenos</a>
                <a href="https://go.microsoft.com/fwlink/?LinkId=521839">Privacidad</a>
                <a href="http://products.office.com/es-ar/products">Todos los productos</a> 
                <a href="http://www.microsoft.com/en-us/legal/intellectualproperty/copyright/default.aspx">Marcas Registradas</a>
                <a href="http://go.microsoft.com/fwlink/p/?LinkID=511200">Sobre nuestra publicidad</a>
            </div>
        </div>
    </footer>   
        <script src="assets/bootstrap.js"></script>
        <script src="assets/app_faqs.js"></script>


        
        <!-- INICIO TRACKING CODE GOOGLE ANALITYCS BA DIGITAL -->
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-84260910-1', 'auto');
          ga('send', 'pageview');

        </script>
        <!-- FIN TRACKING CODE GOOGLE ANALITYCS BA DIGITAL -->
        <!--  INICIO WEDECX & GOOGLE CODE -->
        <script type="text/javascript">
            var varSegmentation = 0;
            var varClickTracking = 1;
            var varCustomerTracking = 1;
            var varAutoFirePV =1;
            var Route="";
            var Ctrl=""
            document.write("<script type='text/javascript' src='" + (window.location.protocol) + "//c.microsoft.com/ms.js'" + "'><\/script>");
        </script>
        <noscript>
        <img src="http://c.microsoft.com/trans_pixel.aspx" id="ctl00_MscomBI_ctl00_ImgWEDCS" width="1" height="1" />
        </noscript>
        <!--  FIN WEDECX & GOOGLE CODE -->
    </body>
</html>